package dv.spring.kotlin.util

import dv.spring.kotlin.entity.*
import dv.spring.kotlin.entity.dto.*
import org.mapstruct.*
import org.mapstruct.factory.Mappers

@Mapper(componentModel = "spring")
interface MapperUtil {
    companion object {
        val INSTANCE = Mappers.getMapper(MapperUtil::class.java)
    }

    @Mappings(
            Mapping(source = "manufacturer", target = "manu")


    )
    fun mapProductDto(product: Product?): ProductDto?
    fun mapProductDto(products: List<Product>): List<ProductDto>
    fun mapManufacturer(manu: Manufacturer): ManufacturerDto


    fun mapCustomerDto(customer: Customer?): CustomerDto?
    fun mapCustomerDto(customers: List<Customer>): List<CustomerDto>

    fun mapShoppingCartDto(shoppingCart: ShoppingCart): ShoppingCartDto
    fun mapShoppingCartDto(shoppingCarts: List<ShoppingCart>): List<ShoppingCartDto>


    fun mapSelectedProductDto(selectedProduct: SelectedProduct?): SelectedProductDto?
    fun mapSelectedProductDto(selectedProducts: List<SelectedProduct>): List<SelectedProductDto>

    fun mapCustomerProductDto(shoppingCart: ShoppingCart): CustomerProductDto
    fun mapCustomerProductDto(shoppingCarts: List<ShoppingCart>): List<CustomerProductDto>

    fun mapAddress(address: Address):AddressDto

    @InheritInverseConfiguration
    fun mapManufacturer(manu: ManufacturerDto): Manufacturer

    @InheritInverseConfiguration
    fun mapAddress(address: AddressDto):Address

    @InheritInverseConfiguration
    fun mapProductDto(productDto: ProductDto): Product

    @InheritInverseConfiguration
    fun mapCustomerDto(customerDto: CustomerDto):Customer

    @InheritInverseConfiguration
    fun mapShoppingCart(shoppingCart: ShoppingCartDto): ShoppingCart

    fun mapMyShoppingCart(shoppingCart: ShoppingCart): MyShoppingCartDto

    fun mapMyShoppingCart(shoppingCart: List<ShoppingCart>): List<MyShoppingCartDto>

    @InheritInverseConfiguration
    fun mapMyShoppingCart(shoppingCart: MyShoppingCartDto): ShoppingCart


}


