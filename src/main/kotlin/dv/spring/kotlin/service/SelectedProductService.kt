package dv.spring.kotlin.service

import dv.spring.kotlin.entity.SelectedProduct
import org.springframework.data.domain.Page

interface SelectedProductService {
    fun getSelectedProducts(): List<SelectedProduct>
    fun getSelectedProductByProduct_Name(product: String): SelectedProduct?
    fun getSelectedProductByPartialName(product: String): List<SelectedProduct>
    fun getSelectedProductByPartialNameAndQuantity(product: String, quantity: Int): List<SelectedProduct>
    fun getSelectedProductWithPage(product: String, page: Int, pageSize: Int): Page<SelectedProduct>

}