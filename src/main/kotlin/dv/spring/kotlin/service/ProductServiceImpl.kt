package dv.spring.kotlin.service

import dv.spring.kotlin.dao.ManufacturerDao
import dv.spring.kotlin.dao.ProductDao
import dv.spring.kotlin.entity.Product
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.stereotype.Service
import javax.transaction.Transactional

@Service
class ProductServiceImpl: ProductService{
    @Transactional
    override fun remove(id: Long): Product? {
        val product = productDao.findById(id)
        product?.isDeleted = true
        return product
    }

    @Autowired
    lateinit var manufacturerDao: ManufacturerDao
    @Transactional
    override fun save(manuId:Long,product: Product): Product {
        val manufacturer = manufacturerDao.findById(manuId)
        val product = productDao.save(product)
        manufacturer?.products?.add(product)
        return product
    }

    override fun getProductWithPage(name: String, page: Int, pageSize: Int): Page<Product> {
        return productDao.getProductWithPage(name,page,pageSize)
    }

    override fun getProductByManuName(name: String): List<Product> {
        return productDao.getProductByManuName(name)
    }

    override fun getProductByPartialNameAndDesc(name: String, desc: String): List<Product> {
        return productDao.getProductByPartialNameDesc(name,desc)
    }

    override fun getProductByPartialName(name: String): List<Product> {
        return productDao.getProductByPartialName(name)
    }

    @Autowired
    lateinit var productDao: ProductDao

    override fun getProductByName(name: String): Product?
        = productDao.getProductByName(name)

    override fun getProducts(): List<Product> {
        return productDao.getProducts()
    }
}