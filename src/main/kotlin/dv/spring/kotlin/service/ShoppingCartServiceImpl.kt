package dv.spring.kotlin.service

import dv.spring.kotlin.dao.CustomerDao
import dv.spring.kotlin.dao.ProductDao
import dv.spring.kotlin.dao.SelectedProductDao
import dv.spring.kotlin.dao.ShoppingCartDao
import dv.spring.kotlin.entity.SelectedProduct
import dv.spring.kotlin.entity.ShoppingCart
import dv.spring.kotlin.entity.dto.MyShoppingCartDto
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.stereotype.Service
import javax.transaction.Transactional

@Service
class ShoppingCartServiceImpl:ShoppingCartService{
    @Autowired
    lateinit var customerDao: CustomerDao
    @Autowired
    lateinit var productDao: ProductDao
    @Autowired
    lateinit var selectedProductDao: SelectedProductDao


    @Transactional
    override fun save(customerId: Long, shoppingCart: MyShoppingCartDto): ShoppingCart {
        var shoppingCartttt = ShoppingCart()
        var listSelectedProduct = mutableListOf<SelectedProduct>()
        var customer = customerDao.findById(customerId)

        for (each in shoppingCart.myproducts) {
            var product = productDao.findById(each.id!!)
            var selected = SelectedProduct(each.quantity)
            selected.product = product

            selectedProductDao.save(selected)
            listSelectedProduct.add(selected)
        }
        shoppingCartttt.customer = customer
        shoppingCartttt.selectedProducts = listSelectedProduct
        shoppingCartDao.save(shoppingCartttt)
        return shoppingCartttt

    }


    override fun getCustomerByProductName(name: String): List<ShoppingCart> {
        return shoppingCartDao.getCustomerByProductName(name)
    }

    override fun getAllShoppingCartWithPage(page: Int, pageSize: Int): Page<ShoppingCart> {
        return shoppingCartDao.getAllShoppingCartWithPage(page,pageSize)
    }


    override fun getShoppingCartWithPage(name: String, page: Int, pageSize: Int): Page<ShoppingCart> {
        return shoppingCartDao.getShoppingCartWithPage(name,page,pageSize)
    }

    override fun getShoppingCartByProductName(name: String): List<ShoppingCart> {
        return shoppingCartDao.getShoppingCartByProductName(name)
    }

    @Autowired
    lateinit var shoppingCartDao: ShoppingCartDao

    override fun getShoppingCarts(): List<ShoppingCart> {
        return shoppingCartDao.getShoppingCarts()
    }

}