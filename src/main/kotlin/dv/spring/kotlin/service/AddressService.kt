package dv.spring.kotlin.service

import dv.spring.kotlin.entity.Address
import dv.spring.kotlin.entity.Customer
import dv.spring.kotlin.entity.dto.AddressDto

interface AddressService{
    fun save(address: AddressDto): Address

}