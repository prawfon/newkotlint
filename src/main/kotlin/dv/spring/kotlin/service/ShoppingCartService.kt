package dv.spring.kotlin.service

import dv.spring.kotlin.entity.ShoppingCart
import dv.spring.kotlin.entity.dto.MyShoppingCartDto
import org.springframework.data.domain.Page

interface ShoppingCartService {

    fun getShoppingCarts(): List<ShoppingCart>
    fun getShoppingCartByProductName(name: String): List<ShoppingCart>
    fun getShoppingCartWithPage(name: String, page: Int, pageSize: Int): Page<ShoppingCart>
    fun getCustomerByProductName(name: String): List<ShoppingCart>
    fun getAllShoppingCartWithPage(page: Int, pageSize: Int): Page<ShoppingCart>
//    fun save(customerId: Long, myShoppingCartDto: MyShoppingCartDto): ShoppingCart
    fun save(customerId: Long, shoppingCart: MyShoppingCartDto): ShoppingCart


}