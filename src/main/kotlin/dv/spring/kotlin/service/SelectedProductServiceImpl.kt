package dv.spring.kotlin.service

import dv.spring.kotlin.dao.SelectedProductDao
import dv.spring.kotlin.entity.SelectedProduct
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.stereotype.Service

@Service
class SelectedProductServiceImpl : SelectedProductService {
    override fun getSelectedProductWithPage(product: String, page: Int, pageSize: Int): Page<SelectedProduct> {
        return selectedProductDao.getSelectedProductWithPage(product,page,pageSize)
    }

    override fun getSelectedProductByPartialNameAndQuantity(product: String, quantity: Int): List<SelectedProduct> {
        return selectedProductDao.getSelectedProductByPartialNameAndQuantity(product, quantity)
    }

    override fun getSelectedProductByPartialName(product: String): List<SelectedProduct> {
        return selectedProductDao.getSelectedProductByPartialName(product)
    }

    override fun getSelectedProductByProduct_Name(product: String): SelectedProduct?
        = selectedProductDao.getSelectedProductByProduct_Name(product)


    @Autowired
    lateinit var selectedProductDao: SelectedProductDao


    override fun getSelectedProducts(): List<SelectedProduct> {
        return selectedProductDao.getSelectedProducts()
    }

}