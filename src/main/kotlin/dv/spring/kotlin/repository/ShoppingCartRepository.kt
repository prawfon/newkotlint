package dv.spring.kotlin.repository


import dv.spring.kotlin.entity.ShoppingCart
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageRequest
import org.springframework.data.domain.Pageable
import org.springframework.data.repository.CrudRepository

interface ShoppingCartRepository : CrudRepository<ShoppingCart, Long> {
    fun findByselectedProducts_Product_nameContainingIgnoreCase(name: String): List<ShoppingCart>
    fun findByselectedProducts_Product_nameContainingIgnoreCase(name: String, page: Pageable): Page<ShoppingCart>
    fun findAll(page: Pageable): Page<ShoppingCart>


}
