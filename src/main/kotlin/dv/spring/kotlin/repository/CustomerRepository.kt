package dv.spring.kotlin.repository

import dv.spring.kotlin.entity.Customer
import dv.spring.kotlin.entity.UserStatus
import org.springframework.data.repository.CrudRepository

interface CustomerRepository: CrudRepository<Customer,Long>{
    fun findByName(name:String): Customer?
    fun findByNameContaining(name: String): List<Customer>
    fun findByNameContainingIgnoreCase(name:String): List<Customer>
    fun findByNameEndingWith(name: String): List<Customer>
    fun findByNameContainingIgnoreCaseOrEmailContainingIgnoreCase(name:String,email:String): List<Customer>
    fun findByDefaultAddress_ProvinceContainingIgnoreCase(province: String?): List<Customer>
    fun findByUserStatus(userStatus: UserStatus): List<Customer>
    fun findByIsDeletedIsFalse():List<Customer>


}
