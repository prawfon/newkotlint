package dv.spring.kotlin.dao

import dv.spring.kotlin.entity.ShoppingCart
import dv.spring.kotlin.repository.ShoppingCartRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Profile
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageRequest
import org.springframework.stereotype.Repository

@Profile("db")
@Repository
class ShoppingCartDaoDBImpl : ShoppingCartDao {
    override fun save(shoppingCart: ShoppingCart): ShoppingCart {
        return shoppingCartRepository.save(shoppingCart)
    }

    override fun getCustomerByProductName(name: String): List<ShoppingCart> {
        return shoppingCartRepository.findByselectedProducts_Product_nameContainingIgnoreCase(name)
    }
    override fun getAllShoppingCartWithPage(page: Int, pageSize: Int): Page<ShoppingCart> {
        return shoppingCartRepository.findAll(PageRequest.of(page,pageSize))
    }

    override fun getShoppingCartWithPage(name: String, page: Int, pageSize: Int): Page<ShoppingCart> {
        return shoppingCartRepository.findByselectedProducts_Product_nameContainingIgnoreCase(name, PageRequest.of(page, pageSize))
    }

    override fun getShoppingCartByProductName(name: String): List<ShoppingCart> {
        return shoppingCartRepository.findByselectedProducts_Product_nameContainingIgnoreCase(name)
    }

    @Autowired
    lateinit var shoppingCartRepository: ShoppingCartRepository

    override fun getShoppingCarts(): List<ShoppingCart> {
        return shoppingCartRepository.findAll().filterIsInstance(ShoppingCart::class.java)
    }
}