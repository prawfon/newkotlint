package dv.spring.kotlin.dao

import dv.spring.kotlin.entity.Customer
import dv.spring.kotlin.entity.UserStatus
import dv.spring.kotlin.repository.CustomerRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Profile
import org.springframework.stereotype.Repository

@Profile("db")
@Repository
class CustomerDaoDBImpl : CustomerDao {
    override fun findById(id: Long): Customer? {
        return customerRepository.findById(id).orElse(null)
    }

    override fun save(customer: Customer): Customer {
      return customerRepository.save(customer)
    }

    override fun getCustomerByPartialUserStatus(userStatus: UserStatus): List<Customer> {
        return customerRepository.findByUserStatus(userStatus)
    }

    override fun getCustomerByProvince(province: String?): List<Customer> {
        return customerRepository.findByDefaultAddress_ProvinceContainingIgnoreCase(province)
    }

    override fun getCustomerByPartialNameAndDesc(name: String, email: String): List<Customer> {
        return customerRepository.findByNameContainingIgnoreCaseOrEmailContainingIgnoreCase(name, email)
    }

    override fun getCustomerByPartialName(name: String): List<Customer> {
        return customerRepository.findByNameEndingWith(name)
    }

    @Autowired
    lateinit var customerRepository: CustomerRepository

    override fun getCustomers(): List<Customer> {
//        return customerRepository.findAll().filterIsInstance(Customer::class.java)
        return customerRepository.findByIsDeletedIsFalse()
    }

    override fun getCustomerByName(name: String): Customer? {
        return customerRepository.findByName(name)
    }



}