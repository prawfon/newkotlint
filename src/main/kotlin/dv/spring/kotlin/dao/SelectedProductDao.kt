package dv.spring.kotlin.dao

import dv.spring.kotlin.entity.SelectedProduct
import org.springframework.data.domain.Page

interface SelectedProductDao {
    fun getSelectedProducts(): List<SelectedProduct>
    fun getSelectedProductByProduct_Name(product: String): SelectedProduct?
    fun getSelectedProductByPartialName(product: String): List<SelectedProduct>
    fun getSelectedProductByPartialNameAndQuantity(product: String, quantity: Int): List<SelectedProduct>
    fun getSelectedProductWithPage(product: String, page: Int, pageSize: Int): Page<SelectedProduct>
    fun save(selectedProduct: SelectedProduct): SelectedProduct
}