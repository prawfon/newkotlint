package dv.spring.kotlin.dao

import dv.spring.kotlin.entity.Address
import org.springframework.beans.factory.annotation.Autowired

interface AddressDao{

    fun save(address: Address): Address
    fun findById(id:Long):Address

}