package dv.spring.kotlin.dao

import dv.spring.kotlin.entity.ShoppingCart
import org.springframework.data.domain.Page

interface ShoppingCartDao {
    fun getShoppingCarts(): List<ShoppingCart>
    fun getShoppingCartByProductName(name: String): List<ShoppingCart>
    fun getShoppingCartWithPage(name: String, page: Int, pageSize: Int): Page<ShoppingCart>
    fun getCustomerByProductName(name: String): List<ShoppingCart>
    fun getAllShoppingCartWithPage(page: Int, pageSize: Int): Page<ShoppingCart>
    fun save(shoppingCart: ShoppingCart):ShoppingCart
}