package dv.spring.kotlin.dao

import dv.spring.kotlin.entity.SelectedProduct
import dv.spring.kotlin.repository.SelectedProductRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Profile
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageRequest
import org.springframework.stereotype.Repository

@Profile("db")
@Repository
class SelectedProductDaoDBImpl : SelectedProductDao {
    override fun save(selectedProduct: SelectedProduct): SelectedProduct {
        return selectedProductRepository.save(selectedProduct)
    }

    override fun getSelectedProductWithPage(product: String, page: Int, pageSize: Int): Page<SelectedProduct> {
        return selectedProductRepository.findByProduct_NameContainingIgnoreCase(product,PageRequest.of(page, pageSize))
    }

    override fun getSelectedProductByPartialNameAndQuantity(product: String, quantity: Int): List<SelectedProduct> {
        return selectedProductRepository.findByProduct_NameContainingIgnoreCaseOrQuantityContaining(product, quantity)
    }

    override fun getSelectedProductByPartialName(product: String): List<SelectedProduct> {
        return selectedProductRepository.findByProduct_NameContainingIgnoreCase(product)
    }


    @Autowired
    lateinit var selectedProductRepository: SelectedProductRepository

    override fun getSelectedProducts(): List<SelectedProduct> {
        return selectedProductRepository.findAll().filterIsInstance(SelectedProduct::class.java)
    }

    override fun getSelectedProductByProduct_Name(product: String): SelectedProduct? {
        return selectedProductRepository.findByProduct_Name(product)
    }


}