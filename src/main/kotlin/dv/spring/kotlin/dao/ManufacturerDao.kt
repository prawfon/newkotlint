package dv.spring.kotlin.dao

import dv.spring.kotlin.entity.Manufacturer

interface  ManufacturerDao{
    fun getManufacturers():List<Manufacturer>
   fun save(manufacturer: Manufacturer): Manufacturer
   fun findById(id: Long): Manufacturer
}