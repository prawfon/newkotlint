package dv.spring.kotlin.entity.dto


import dv.spring.kotlin.entity.ShoppingCartStatus

data class ShoppingCartDto(var shoppingCartStatus: ShoppingCartStatus = ShoppingCartStatus.SENT,
                           var customer: CustomerDto? = null,
                           var selectedProducts: List<SelectedProductDto>? = null)