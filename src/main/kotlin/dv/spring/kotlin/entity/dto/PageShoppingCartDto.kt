package dv.spring.kotlin.entity.dto

data class PageShoppingCartDto(var totalPages: Int? = null,
                               var totalElements: Long? = null,
                               var shoppingCarts: List<ShoppingCartDto> = mutableListOf())