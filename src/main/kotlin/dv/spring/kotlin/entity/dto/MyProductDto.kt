package dv.spring.kotlin.entity.dto
data class MyProductDto (
        var id:Long? = null,
        var quantity:Int? = null
)
