package dv.spring.kotlin.entity.dto

data class MyShoppingCartDto (
        var myproducts: List<MyProductDto> = mutableListOf()
)
