package dv.spring.kotlin.entity.dto

import dv.spring.kotlin.entity.Address
import dv.spring.kotlin.entity.User
import dv.spring.kotlin.entity.UserStatus

data class CustomerDto( var name: String? = null,
                        var email: String? = null ,
                        var userStatus: UserStatus? = UserStatus.PEDING,
                        var defaultAddress: Address? = null,
//                        var shippingAddress: List<Address>? = null,
//                        var billingAddress: Address? = null,
                        var id:Long? = null
)