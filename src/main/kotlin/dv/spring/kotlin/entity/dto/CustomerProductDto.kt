package dv.spring.kotlin.entity.dto

data class CustomerProductDto(
        var customer: CustomerDto? = null
)