package dv.spring.kotlin.entity

import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id
import javax.persistence.OneToOne

@Entity
data class Address (var homeAddress: String? = null,
                    var subdistrict: String? = null,
                    var district: String? = null,
                    var province: String? = null,
                    var postCode: String? = null){
    @Id
    @GeneratedValue
    var id:Long? = null

}