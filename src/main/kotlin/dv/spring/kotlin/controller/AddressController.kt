package dv.spring.kotlin.controller

import dv.spring.kotlin.entity.dto.AddressDto
import dv.spring.kotlin.service.AddressService
import dv.spring.kotlin.util.MapperUtil
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@RestController
class AddressController {
    @Autowired
    lateinit var addressService: AddressService

    @PostMapping("/address")
    fun addAddress(@RequestBody address: AddressDto): ResponseEntity<Any> {
        return ResponseEntity.ok(MapperUtil.INSTANCE.mapAddress(addressService.save(address)))
    }

    @PutMapping("/address/{addressId}")
    fun updateAddress(@PathVariable("addressId") id:Long?,
                      @RequestBody address: AddressDto):ResponseEntity<Any>{
        address.id = id
        return ResponseEntity.ok(MapperUtil.INSTANCE.mapAddress(addressService.save(address)))
    }
}